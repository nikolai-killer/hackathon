import 'package:flutter/material.dart';

const studProfileTextStyle = TextStyle(fontStyle: FontStyle.italic, fontSize: 30);
const studProfileRoleStyle = TextStyle(fontStyle: FontStyle.italic, fontSize: 20);