import 'package:azure_cosmosdb/azure_cosmosdb.dart';
import 'package:uuid/uuid.dart';
import 'dart:convert';

class Thesis extends BaseDocumentWithEtag  {
  Thesis._(
      this.id,
      this.topic,
      this.description,
      this.fb,
      this.type,
      this.imageLink,
      this.applicants,
      this.creator,
      );

  Thesis({
    String? id,
    required String topic,
    required String description,
    required String fb,
    required String type,
    required String imageLink,
    required List<String> applicants,
    required String creator
  }) : this._(id ?? const Uuid().v1(), topic, description, fb, type, imageLink, applicants, creator);

  @override
  final String id;
  String topic;
  String description;
  String fb;
  String type;
  String imageLink;
  List<String> applicants;
  String creator;

  @override
  Map<String, dynamic> toJson() => {
    'id': id,
    'topic': topic,
    'description': description,
    'fb': fb,
    'type': type,
    'imageLink': imageLink,
    'applicants': json.encode(applicants),
    'creator': creator,
  };

  static Thesis fromJson(Map jsonMap) {
    final thesis = Thesis._(
      jsonMap['id'] ?? '',
      jsonMap['topic'],
      jsonMap['description'],
      jsonMap['fb'],
      jsonMap['type'],
      jsonMap['imageLink'],
      List<String>.from(json.decode(jsonMap['applicants'])),
      jsonMap['creator'],
    );
    thesis.setEtag(jsonMap);
    return thesis;
  }

  @override
  String toString() => '$topic, $description, $fb, $type, $imageLink, $applicants, $creator';
}