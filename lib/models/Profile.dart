import 'package:azure_cosmosdb/azure_cosmosdb.dart';
import 'package:uuid/uuid.dart';

class Profile extends BaseDocumentWithEtag  {
  Profile._(
      this.id,
      this.email,
      this.name,
      this.type,
      this.password,
      );

  Profile({
    String? id,
    required String email,
    required String name,
    required String type,
    required String password
  }) : this._(id ?? const Uuid().v1(), email, name, type, password);

  @override
  final String id;
  String email;
  String name;
  String type;
  String password;

  @override
  Map<String, dynamic> toJson() => {
    'id': id,
    'email': email,
    'name': name,
    'type': type,
    'password': password,
  };

  static Profile fromJson(Map json) {
    final profile = Profile._(
      json['id'] ?? '',
      json['email'],
      json['name'],
      json['type'],
      json['password'],
    );
    profile.setEtag(json);
    return profile;
  }

  @override
  String toString() => '$name, $type, $password';

}