import 'package:azure_cosmosdb/azure_cosmosdb_debug.dart';
import 'package:thinder/models/Profile.dart';
import 'package:collection/collection.dart';
import 'package:thinder/models/Thesis.dart';

class DatabaseService{
  final cosmosDB = CosmosDbServer(
    'https://cosmosdb-hackathon-t3.documents.azure.com:443/',
    masterKey: "dbfGg2cwkMFI4P2yv8UpDmfoSti0SIUmCCNxwl3knCnWReD8lE1BZP0bjYCg8tqeZJVQikRfZxwvACDbQ8nurQ==",
  );
  CosmosDbDatabase? _database;
  CosmosDbContainer? _thesisContainer;
  CosmosDbContainer? _profileContainer;

  DatabaseService()
  {
    createConnections();
  }

  Future<void> createConnections() async {
    final createdDB = await cosmosDB.databases.openOrCreate(
      'thinder',
      throughput: CosmosDbThroughput.minimum
    );
    _database = createdDB;
    createThesisContainer();
    createProfileContainer();
  }

  Future<void> createThesisContainer() async {
    final thesisContainer = await _database!.containers.openOrCreate(
      'Thesis',
      partitionKey: PartitionKeySpec('/fb'),
    );
    _thesisContainer = thesisContainer;
    print('created:');
    print(_thesisContainer);
  }

  Future<void> createProfileContainer() async {
    final profileContainer = await _database!.containers.openOrCreate(
      'Profile',
      partitionKey: PartitionKeySpec.id,
    );
    _profileContainer = profileContainer;
  }

  Future<Profile?> readProfile(String email, String password) async {
    _profileContainer!.registerBuilder<Profile>(Profile.fromJson);
    final Profile? profile = (await _profileContainer!.query<Profile>(Query('SELECT * FROM c WHERE c.email = @email AND c.password = @password', params: { '@email': email, '@password': password }))).firstOrNull;
    return profile;
  }

  Future<void> createProfile(Profile profile) async {
    try {
      await _profileContainer!.add<Profile>(profile);
    } catch(e) {
     print('not sure if added');
    }
  }

  Future<List<Thesis>> queryThesisByCompany(String companyEmail) async {
    _thesisContainer!.registerBuilder<Thesis>(Thesis.fromJson);
    return (await _thesisContainer!.query<Thesis>(Query('SELECT * FROM c WHERE c.creator = @creator ', params: { '@creator': companyEmail }))).toList();
  }

  Future<List<Thesis>> queryThesis(String fb) async {
    _thesisContainer!.registerBuilder<Thesis>(Thesis.fromJson);
    return (await _thesisContainer!.query<Thesis>(Query('SELECT * FROM c WHERE c.fb = @fb ', params: { '@fb': fb }))).toList();
  }

  Future<void> addApplicant(Thesis thesis, String email) async {
    thesis.applicants.add(email);
    _thesisContainer!.registerBuilder<Thesis>(Thesis.fromJson);
    try {
      await _thesisContainer!.replace<Thesis>(thesis);
    } catch(e) {
      print('not sure if added');
    }
  }

  Future<List<Thesis>> receiveAllThesis() async {
    _thesisContainer!.registerBuilder<Thesis>(Thesis.fromJson);
    return (await _thesisContainer!.query<Thesis>(Query('SELECT * FROM c '))).toList();
  }

  Future<void> createThesis(Thesis thesis) async {
    _thesisContainer!.registerBuilder<Thesis>(Thesis.fromJson);
    try {
      await _thesisContainer!.add(thesis);
    } catch(e) {
      print('not sure if added');
    }
  }
}