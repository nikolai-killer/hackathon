import 'package:flutter/material.dart';

import '../database/dataBaseService.dart';
import '../models/Thesis.dart';
import '../services/getIt.dart';

class CompanyView extends StatefulWidget {
  const CompanyView({Key? key}) : super(key: key);

  @override
  State<CompanyView> createState() => _CompanyViewState();
}

class _CompanyViewState extends State<CompanyView> {
  final DatabaseService _databaseService = getIt.get<DatabaseService>();
  List<Thesis> thesisList = List.empty(growable: true);
  
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_){
      loadThesisList();
    });
  }

  Future<void> loadThesisList() async {
    List<Thesis> fetchedList = await _databaseService.queryThesisByCompany("creatorMode");
    setState(() {
      thesisList.addAll(fetchedList);
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          const SizedBox(height: 10,),
          const Center(
            child: Text(
              "Thesis Übersicht",
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          for (var element in thesisList)
            createExpansionTile(element.topic, element.applicants),
        ],
      ),
    );
  }

  Widget createExpansionTile(String title, List<String> applicants) {

    return ExpansionTile(
      title: Text(title),
      children: <Widget>[
        for (var element in applicants)
          Column(children: [Text(element), const SizedBox(height: 5,)],)
      ],
    );
  }
}
