import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'company_view.dart';
import 'company_add_thesis.dart';

@RoutePage()
class CompanyHome extends StatefulWidget {
  const CompanyHome({Key? key}) : super(key: key);

  @override
  State<CompanyHome> createState() => _CompanyHomeState();
}

class _CompanyHomeState extends State<CompanyHome> {
  int _selectedIndex = 0;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  void goToFirstPage () {
    setState(() {
      _selectedIndex = 0 ;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //appBar: AppBar(),
        body: Center(
          child: IndexedStack(
            index: _selectedIndex,
            children: <Widget>[
              CompanyView(),
              CompanyAddThesis(goToFirstPage: goToFirstPage,),
            ],
          ),
        ),
        bottomNavigationBar: _bottomNavBar(_selectedIndex, _onItemTapped)
    );
  }
}

Widget _bottomNavBar(int selectedIndex, void Function(int index) onItemTapped) {
  return BottomNavigationBar(
    items: const <BottomNavigationBarItem>[
      BottomNavigationBarItem(
        icon: Icon(Icons.list),
        label: 'Thesis Übersicht',
        backgroundColor: Colors.red,
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.note_add),
        label: 'Thesis Hinzufügen',
        backgroundColor: Colors.green,
      ),
    ],
    currentIndex: selectedIndex,
    selectedItemColor: Colors.blue,
    onTap: onItemTapped,
  );
}
