import 'package:flutter/material.dart';
import 'package:thinder/database/dataBaseService.dart';
import 'package:thinder/services/getIt.dart';

import '../models/Thesis.dart';

class CompanyAddThesis extends StatefulWidget {
final void Function () goToFirstPage ;
 CompanyAddThesis({required this.goToFirstPage,Key? key}) : super(key: key);

  @override
  State<CompanyAddThesis> createState() => _CompanyAddThesisState();
}

const List<String> thesisTypes = <String>['Bachelor', 'Master'];

const List<String> departments = <String>[
  'Informatik',
  'BWL',
  'Elektrotechnik',
  'Maschinenbau',
  'Soziale Arbeit',
  'Physik'
];

class _CompanyAddThesisState extends State<CompanyAddThesis> {
  final DatabaseService _databaseService = getIt.get<DatabaseService>();
  String _selectedThesis = thesisTypes.first;
  String _selectedDepartment = departments.first;

  TextEditingController topicController = new TextEditingController();
  TextEditingController descriptionController = new TextEditingController();
  TextEditingController imageLinkController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: SingleChildScrollView(
        child: Form(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Center(
                child: Text(
                  "Thesis Hinzufügen",
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              SizedBox(height: 16.0),
              makeDropdown((String value) {
                _selectedThesis = value;
              },
                  label: 'Typ',
                  dropdownValues: thesisTypes,
                  currentValue: _selectedThesis),
              makeDropdown((String value) {
                _selectedDepartment = value;
              },
                  label: 'Fachbereich',
                  dropdownValues: departments,
                  currentValue: _selectedDepartment),
              makeInput(label: 'Titel', textController: topicController),
              makeInput(
                  label: 'Beschreibung', textController: descriptionController),
              makeInput(
                  label: 'Bild Link', textController: imageLinkController),
              MaterialButton(
                minWidth: double.infinity,
                height: 60,
                onPressed: () async {
                  if (topicController.text == '' ||
                      descriptionController.text == '' ||
                      imageLinkController.text == '') {
                    _emptyErrorDialog();
                  } else {
                    // do something with the values from the form...
                    Thesis myThesis = Thesis(topic: topicController.text, description: descriptionController.text, fb: _selectedDepartment, type: _selectedThesis, imageLink: imageLinkController.text, applicants: <String>[], creator: 'creatorMode');
                    await _databaseService.createThesis(myThesis);
                    widget.goToFirstPage();

                  }
                },
                color: Colors.indigoAccent[400],
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(40)),
                child: Text(
                  "Absenden",
                  style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 16,
                      color: Colors.white70),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _emptyErrorDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Bitte alle Felder ausfüllen!'),
          content: SingleChildScrollView(
            child: ListBody(
              children: const <Widget>[
                Text(''),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget makeDropdown(void Function(String currentValue) func,
      {label, dropdownValues, currentValue}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          label,
          style: const TextStyle(
              fontSize: 15, fontWeight: FontWeight.w400, color: Colors.black87),
        ),
        const SizedBox(
          height: 5,
        ),
        DropdownButton<String>(
          value: currentValue,
          elevation: 16,
          isExpanded: true,
          onChanged: (String? value) {
            setState(() {
              if (value != null) {
                func(value);
              }
            });
          },
          items: dropdownValues.map<DropdownMenuItem<String>>((String value) {
            return DropdownMenuItem<String>(
              value: value,
              child: Text(value),
            );
          }).toList(),
        ),
        const SizedBox(
          height: 30,
        )
      ],
    );
  }
}

Widget makeInput({label, required TextEditingController textController}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Text(
        label,
        style: TextStyle(
            fontSize: 15, fontWeight: FontWeight.w400, color: Colors.black87),
      ),
      SizedBox(
        height: 5,
      ),
      TextField(
        keyboardType: TextInputType.multiline,
        maxLines: null,
        controller: textController,
        decoration: InputDecoration(
          //contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
          contentPadding: EdgeInsets.all(20.0),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: (Colors.grey[400])!,
            ),
          ),
          border: OutlineInputBorder(
              borderSide: BorderSide(color: (Colors.grey[400])!)),
        ),
      ),
      SizedBox(
        height: 30,
      )
    ],
  );
}
