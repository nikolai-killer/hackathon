// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:auto_route/auto_route.dart' as _i5;
import 'package:thinder/auth/login.dart' as _i1;
import 'package:thinder/auth/register.dart' as _i2;
import 'package:thinder/company_view/company_home.dart' as _i3;
import 'package:thinder/student_view/student_home.dart' as _i4;

abstract class $AppRouter extends _i5.RootStackRouter {
  $AppRouter({super.navigatorKey});

  @override
  final Map<String, _i5.PageFactory> pagesMap = {
    LoginRoute.name: (routeData) {
      return _i5.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i1.LoginPage(),
      );
    },
    RegisterView.name: (routeData) {
      return _i5.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i2.RegisterView(),
      );
    },
    CompanyHome.name: (routeData) {
      return _i5.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i3.CompanyHome(),
      );
    },
    StudentHome.name: (routeData) {
      return _i5.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i4.StudentHome(),
      );
    },
  };
}

/// generated route for
/// [_i1.LoginPage]
class LoginRoute extends _i5.PageRouteInfo<void> {
  const LoginRoute({List<_i5.PageRouteInfo>? children})
      : super(
          LoginRoute.name,
          initialChildren: children,
        );

  static const String name = 'LoginRoute';

  static const _i5.PageInfo<void> page = _i5.PageInfo<void>(name);
}

/// generated route for
/// [_i2.RegisterView]
class RegisterView extends _i5.PageRouteInfo<void> {
  const RegisterView({List<_i5.PageRouteInfo>? children})
      : super(
          RegisterView.name,
          initialChildren: children,
        );

  static const String name = 'RegisterView';

  static const _i5.PageInfo<void> page = _i5.PageInfo<void>(name);
}

/// generated route for
/// [_i3.CompanyHome]
class CompanyHome extends _i5.PageRouteInfo<void> {
  const CompanyHome({List<_i5.PageRouteInfo>? children})
      : super(
          CompanyHome.name,
          initialChildren: children,
        );

  static const String name = 'CompanyHome';

  static const _i5.PageInfo<void> page = _i5.PageInfo<void>(name);
}

/// generated route for
/// [_i4.StudentHome]
class StudentHome extends _i5.PageRouteInfo<void> {
  const StudentHome({List<_i5.PageRouteInfo>? children})
      : super(
          StudentHome.name,
          initialChildren: children,
        );

  static const String name = 'StudentHome';

  static const _i5.PageInfo<void> page = _i5.PageInfo<void>(name);
}
