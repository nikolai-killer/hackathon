import 'package:auto_route/auto_route.dart';
import 'package:thinder/services/router.gr.dart';

@AutoRouterConfig()
class AppRouter extends $AppRouter {

  @override
  List<AutoRoute> get routes => [
    AutoRoute(page: LoginRoute.page, initial: true),
    AutoRoute(page: RegisterView.page),
    AutoRoute(page: StudentHome.page),
    AutoRoute(page: CompanyHome.page)
  ];
}