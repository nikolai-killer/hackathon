import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefs {

  static final String NAME_KEY = 'user_name';
  static final String EMAIL_KEY = 'user_email';
  static final String TYPE_KEY = 'user_type';

  static late SharedPreferences _preferences;

  static Future<void> init() async {
    _preferences = await SharedPreferences.getInstance();
  }

  static Future<String> getString(String key) async {
    return _preferences.getString(key) ?? '';
  }

  static Future<void> setString(String key, String value) async {
    await _preferences.setString(key, value);
  }
}