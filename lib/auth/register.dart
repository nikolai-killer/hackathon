import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:thinder/services/router.gr.dart';
import 'package:thinder/services/sharedPref.dart';

import '../database/dataBaseService.dart';
import '../models/Profile.dart';
import '../services/getIt.dart';

@RoutePage()
class RegisterView extends StatefulWidget {
  const RegisterView({Key? key}) : super(key: key);

  @override
  State<RegisterView> createState() => _RegisterViewState();
}

TextEditingController MailController = new TextEditingController();
TextEditingController NameController = new TextEditingController();
TextEditingController PasswortController = new TextEditingController();
TextEditingController FachbereichController = new TextEditingController();
TextEditingController StatusController = new TextEditingController();

const List<String> userTypes = <String>['Student', 'Firma/Professor'];
String _selectedUser = userTypes.first;

class _RegisterViewState extends State<RegisterView> {

  final DatabaseService _databaseService = getIt.get<DatabaseService>();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: SafeArea(
        child: SingleChildScrollView(
          child: SizedBox(
            height: MediaQuery.of(context).size.height,
            width: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: const [
                        SizedBox(
                          height: 20,
                        ),
                        Text(
                          "Sign up",
                          style: TextStyle(
                            fontSize: 30,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 40),
                      child: Column(
                        children: [
                          makeInput(
                              label: "Email", TextController: MailController),
                          makeInput(
                              label: "Name", TextController: NameController),
                          makeInput(
                              label: "Password",
                              obsureText: true,
                              TextController: PasswortController),
                          //       makeInput(label: "Fachbereich",TextController: FachbereichController),
                          makeDropdown((String value) {
                            _selectedUser = value;
                          },
                              label: "Status",
                              dropdownValues: userTypes,
                              currentValue: _selectedUser)
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 40),
                      child: Container(
                        padding: const EdgeInsets.only(top: 3, left: 3),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(40),
                            border: const Border(
                                bottom: BorderSide(color: Colors.black),
                                top: BorderSide(color: Colors.black),
                                right: BorderSide(color: Colors.black),
                                left: BorderSide(color: Colors.black))),
                        child: MaterialButton(
                          minWidth: double.infinity,
                          height: 60,
                          onPressed: () {
                            String mailString = MailController.text;
                            String nameString = NameController.text;
                            String passwortString = PasswortController.text;
                            String statusString = _selectedUser;

                            if (mailString.isEmpty ||
                                nameString.isEmpty ||
                                passwortString.isEmpty ||
                                statusString.isEmpty) {
                              _emptyErrorDialog();
                              return;
                            }
                            else
                              {
                                Profile newProfile = Profile(
                                    email: mailString,
                                    name: nameString,
                                    type: statusString,
                                    password: passwortString);

                                _databaseService.createProfile(newProfile);
                                SharedPrefs.setString(SharedPrefs.EMAIL_KEY, mailString);
                                SharedPrefs.setString(SharedPrefs.NAME_KEY, nameString);
                                SharedPrefs.setString(SharedPrefs.TYPE_KEY, statusString);
                                if(statusString == 'Student') {
                                  context.router.replace(const StudentHome());
                                } else {
                                  context.router.replace(const CompanyHome());
                                }
                              }
                          },
                          color: Colors.redAccent,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(40)),
                          child: const Text(
                            "Sign Up",
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 16,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> _emptyErrorDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Bitte alle Felder ausfüllen!'),
          content: SingleChildScrollView(
            child: ListBody(
              children: const <Widget>[
                Text(''),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  final nameController = TextEditingController();

  Widget makeDropdown(void Function(String currentValue) func,
      {label, dropdownValues, currentValue}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          label,
          style: const TextStyle(
              fontSize: 15, fontWeight: FontWeight.w400, color: Colors.black87),
        ),
        const SizedBox(
          height: 5,
        ),
        DropdownButton<String>(
          value: currentValue,
          elevation: 16,
          isExpanded: true,
          onChanged: (String? value) {
            setState(() {
              if (value != null) {
                func(value);
              }
            });
          },
          items: dropdownValues.map<DropdownMenuItem<String>>((String value) {
            return DropdownMenuItem<String>(
              value: value,
              child: Text(value),
            );
          }).toList(),
        ),
        const SizedBox(
          height: 30,
        )
      ],
    );
  }
}

Widget makeInput(
    {label, obsureText = false, TextEditingController? TextController}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Text(
        label,
        style: const TextStyle(
            fontSize: 15, fontWeight: FontWeight.w400, color: Colors.black87),
      ),
      const SizedBox(
        height: 5,
      ),
      TextField(
        controller: TextController,
        obscureText: obsureText,
        decoration: const InputDecoration(
          contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.grey,
            ),
          ),
          border:
              OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
        ),
      ),
      const SizedBox(
        height: 30,
      )
    ],
  );
}
