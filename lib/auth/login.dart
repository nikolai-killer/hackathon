import 'package:auto_route/auto_route.dart';
import 'package:azure_cosmosdb/azure_cosmosdb.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:thinder/database/dataBaseService.dart';
import 'package:thinder/models/Thesis.dart';
import 'package:thinder/services/getIt.dart';
import 'package:thinder/services/router.gr.dart';
import '../services/sharedPref.dart';

import '../models/Profile.dart';

@RoutePage()
class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final DatabaseService _databaseService = getIt.get<DatabaseService>();
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: SizedBox(
        height: MediaQuery
            .of(context)
            .size
            .height,
        width: double.infinity,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 40),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: 200,
                  child: kIsWeb? Image.network('./assets/assets/logo.jpg',) : Image.asset('logo.jpg')
              ),
              makeInput(
                  label: "Email", textController: _nameController),
              makeInput(label: 'Passwort', textController: _passwordController, obscure: true),
              Container(
                padding: const EdgeInsets.only(top: 3, left: 3),
                child: MaterialButton(
                  minWidth: double.infinity,
                  height: 60,
                  onPressed: () {
                    String name = _nameController.text;
                    String password = _passwordController.text;
                    if (name.isEmpty || password.isEmpty) {
                      _emptyErrorDialog(context);
                      return;
                    }
                    login(name, password, context);
                  },
                  color: Colors.blue,
                  child: const Text(
                    "Einloggen",
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 16,
                        color: Colors.white70),
                  ),
                ),
              ),
              const SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  const Text('Noch nicht dabei?'),
                  TextButton(onPressed: () {
                    context.router.push(const RegisterView());
                  }, child: const Text('Jetzt Registrieren'))
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  void login(String email, String password, BuildContext context) async {
    Profile? profile = await _databaseService.readProfile(email, password);
    if(profile != null) {
      // successfull login
      SharedPrefs.setString(SharedPrefs.EMAIL_KEY, profile.email);
      SharedPrefs.setString(SharedPrefs.NAME_KEY, profile.name);
      SharedPrefs.setString(SharedPrefs.TYPE_KEY, profile.type);
      if(!mounted) return;
      if(profile.type == 'Student') {
        context.router.replace(const StudentHome());
      } else {
        context.router.replace(const CompanyHome());
      }
    } else {
      if(!mounted) return;
      _wrongCredetials(context);
    }
  }
}

Widget makeInput({label, required TextEditingController textController, bool obscure=false}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Text(
        label,
        style: const TextStyle(
            fontSize: 15, fontWeight: FontWeight.w400, color: Colors.black87),
      ),
      const SizedBox(
        height: 5,
      ),
      TextField(
        obscureText: obscure,
        controller: textController,
        decoration: InputDecoration(
          contentPadding: const EdgeInsets.symmetric(vertical: 0, horizontal: 10),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: (Colors.grey[400])!,
            ),
          ),
          border: OutlineInputBorder(
              borderSide: BorderSide(color: (Colors.grey[400])!)),
        ),
      ),
      const SizedBox(
        height: 30,
      )
    ],
  );
}

Future<void> _wrongCredetials(BuildContext context) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: const Text('Email oder Passwort falsch!'),
        actions: <Widget>[
          TextButton(
            child: const Text('Ok'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}

Future<void> _emptyErrorDialog(BuildContext context) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: const Text('Felder nicht ausgefüllt!'),
        content: SingleChildScrollView(
          child: ListBody(
            children: const <Widget>[
              Text('Bitte Benutzername und Passwort ausfüllen.'),
            ],
          ),
        ),
        actions: <Widget>[
          TextButton(
            child: const Text('Ok'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}