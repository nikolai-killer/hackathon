import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:thinder/student_view/student_profile.dart';
import 'package:thinder/student_view/thesisCards.dart';


@RoutePage()
class StudentHome extends StatefulWidget {
  const StudentHome({Key? key}) : super(key: key);

  @override
  State<StudentHome> createState() => _StudentHomeState();
}

class _StudentHomeState extends State<StudentHome> {
  int _selectedIndex = 0;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: IndexedStack(
          index: _selectedIndex,
          children: const <Widget>[
            //StudentThinder(),
            MyHomePage(title: 'Thesen Auswahl'),
            StudentProfile(),
          ],
        ),
      ),
      bottomNavigationBar: _bottomNavBar(_selectedIndex, _onItemTapped)
    );
  }
}

Widget _bottomNavBar(int selectedIndex, void Function(int index) onItemTapped) {
  return BottomNavigationBar(
    items: const <BottomNavigationBarItem>[
      BottomNavigationBarItem(
        icon: Icon(Icons.history_edu),
        label: 'Thinder',
        backgroundColor: Colors.red,
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.account_circle),
        label: 'Profile',
        backgroundColor: Colors.green,
      ),
    ],
    currentIndex: selectedIndex,
    selectedItemColor: Colors.blue,
    onTap: onItemTapped,
  );
}
