import 'package:thinder/database/dataBaseService.dart';
import 'package:thinder/services/getIt.dart';
import 'package:flutter/material.dart';
import 'package:swipe_cards/draggable_card.dart';
import 'package:swipe_cards/swipe_cards.dart';
import 'package:thinder/services/sharedPref.dart';

import '../models/Thesis.dart';

class MyHomePage extends StatefulWidget {

 const  MyHomePage({Key? key, this.title}) : super(key: key);

 final String? title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final DatabaseService _databaseService = getIt.get<DatabaseService>();
  final List<SwipeItem> _swipeItems = <SwipeItem>[];
  MatchEngine? _matchEngine;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  String? useremail;

  bool stackFinished = false;

  Future<void> loadEmail() async {
    String email = await SharedPrefs.getString(SharedPrefs.EMAIL_KEY);
    setState(() {
      useremail = email;
    });
  }

  final List<String> _names = [
    "Red",
    "Blue",
    "Green",
    "Yellow",
    "Orange",
    "Grey",
    "Purple",
    "Pink"
  ];

  final List<Color> _colors = [
    Colors.red,
    Colors.blue,
    Colors.green,
    Colors.yellow,
    Colors.orange,
    Colors.grey,
    Colors.purple,
    Colors.pink
  ];

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_){
      loadEmail();
      loadThesis();
    });
  }

  Future<void> loadThesis() async {
    List<Thesis> loadedThesis = await _databaseService.receiveAllThesis();
    for (int i = 0; i < loadedThesis.length; i++) {
      _swipeItems.add(SwipeItem(
          content: loadedThesis[i],
          likeAction: () {
            _databaseService.addApplicant(loadedThesis[i], useremail ?? '');
          },
          nopeAction: () {},
          superlikeAction: () {
            _databaseService.addApplicant(loadedThesis[i], useremail ?? '');
          },
          onSlideUpdate: (SlideRegion? region) async {}));
    }
    setState(() {
      _matchEngine = MatchEngine(swipeItems: _swipeItems);
    });
  }

  @override
  Widget build(BuildContext context) {

    final double screenWidth = MediaQuery.of(context).size.width;
    final double screenHeight = MediaQuery.of(context).size.height;

    return Scaffold(
        key: _scaffoldKey,
        floatingActionButton: Stack(
          children: <Widget>[
            Container(
              margin: const EdgeInsets.only(left: 40),
              child: Align(
                alignment: Alignment.bottomLeft,
                child: FloatingActionButton(
                  heroTag: "btn1",
                  foregroundColor: Colors.red,
                  backgroundColor: Colors.grey,
                  onPressed: () {
                    _matchEngine!.currentItem?.nope();
                  },
                  child: const Icon(Icons.horizontal_rule, size: 40,),
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(right: 10),
              child: Align(
                alignment: Alignment.bottomRight,
                child: FloatingActionButton(
                  heroTag: "btn2",
                  foregroundColor: Colors.green,
                  backgroundColor: Colors.grey,
                  onPressed: () {
                    _matchEngine!.currentItem?.like();
                  },
                  child: const Icon(Icons.favorite, size: 30,),
                ),
              ),
            ),
          ],
        ),
        body: _matchEngine == null || stackFinished? getIconWidget(): Stack(children: [
          SizedBox(
            height: MediaQuery.of(context).size.height - kToolbarHeight,
            child: SwipeCards(
              matchEngine: _matchEngine!,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  margin: EdgeInsets.symmetric(
                    horizontal: screenWidth * 0.03,
                    vertical: screenHeight * 0.02,
                  ),
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.black,
                      width: 2,
                    ),
                    borderRadius: BorderRadius.circular(30.0),
                    boxShadow: const [
                      BoxShadow(
                        color: Colors.grey,
                        spreadRadius: 5,
                        blurRadius: 5.0,
                        offset: Offset(0, 3), // changes position of shadow
                      ),
                    ],
                    color:Colors.blue,
                    gradient: const LinearGradient(
                      colors: [Colors.blue, Color.fromARGB(255, 220, 221, 220)],
                    ),

                  ),
                  alignment: Alignment.topCenter,
                 // color: _swipeItems[index].content.color,
                child:  Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Image.network(_swipeItems[index].content.imageLink,
                      width: 175, // 50% der Bildschirmbreite
                      height: 175, // 30% der Bildschirmhöhe
                      fit: BoxFit.cover),
                      Padding(
                        padding: EdgeInsets.only(left: 20.0), // füge 20 Einheiten Abstand zum linken Rand hinzu
                        child: Text(
                          "Thema: ${_swipeItems[index].content.topic}",
                          style: const TextStyle(fontSize: 28, fontWeight: FontWeight.bold),
                          textAlign: TextAlign.left,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 20.0),
                        child: Text(
                          "Fachbereich: ${_swipeItems[index].content.fb}",
                          style: const TextStyle(fontSize: 20),
                          textAlign: TextAlign.start,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 20.0),
                        child: Text(
                          "Anbieter: ${_swipeItems[index].content.creator}",
                          style: const TextStyle(fontSize: 20),
                          textAlign: TextAlign.left,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 20.0),
                        child: Text(
                          "Typ: ${_swipeItems[index].content.type}",
                          style: const TextStyle(fontSize: 20),
                          textAlign: TextAlign.left,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 20.0),
                        child: Text(
                          "Beschreibung: ${_swipeItems[index].content.description}",
                          style: const TextStyle(fontSize: 14),
                          textAlign: TextAlign.left,
                        ),
                      ),
                    ],
                  ),
                );
              },
              onStackFinished: () {
                setState(() {
                  stackFinished = true;
                });
              },
              itemChanged: (SwipeItem item, int index) {},
              leftSwipeAllowed: true,
              rightSwipeAllowed: true,
              upSwipeAllowed: true,
              fillSpace: true,
              likeTag: Container(
                margin: const EdgeInsets.all(15.0),
                padding: const EdgeInsets.all(3.0),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.green)
                ),
                child: const Text('Like'),
              ),
              nopeTag: Container(
                margin: const EdgeInsets.all(15.0),
                padding: const EdgeInsets.all(3.0),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.red)
                ),
                child: const Text('Nope'),
              ),
              superLikeTag: Container(
                margin: const EdgeInsets.all(15.0),
                padding: const EdgeInsets.all(3.0),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.orange)
                ),
                child: const Text('Super Like'),
              ),
            ),
          ),
        ]));
  }

  Widget getIconWidget() {
    return Center(
      child: SizedBox(
        height: 200,
        child: Image.asset('./assets/assets/logo.jpg')
      ),
    );
  }
}