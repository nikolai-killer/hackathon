import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:thinder/constants/textstyles.dart';

import '../services/sharedPref.dart';

class StudentProfile extends StatefulWidget {
  const StudentProfile({Key? key}) : super(key: key);

  @override
  State<StudentProfile> createState() => _StudentProfileState();
}

class _StudentProfileState extends State<StudentProfile> {

  ImageProvider _imageProvider = const NetworkImage('https://picsum.photos/200');

  String name = "";
  String email = "";
  String type = "";

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_){
      profile_view();
    });
  }

  void profile_view() async {
    String name_ = await SharedPrefs.getString(SharedPrefs.NAME_KEY);
    String email_ = await SharedPrefs.getString(SharedPrefs.EMAIL_KEY);
    String type_ = await SharedPrefs.getString(SharedPrefs.TYPE_KEY);
    setState(() {
      name = name_;
      email = email_;
      type = type_;
    });
  }


  @override
  Widget build(BuildContext context) {


    return Padding(
      padding: const EdgeInsets.fromLTRB(10, 30, 10, 0),
      child: Column(
        children: [
          SizedBox(
            height: 200,
            width: 200,
            child: CircleAvatar(
              backgroundImage: _imageProvider,
            ),
          ),
          const SizedBox(height: 5,),
          TextButton.icon(
              style: TextButton.styleFrom(
                foregroundColor: Colors.grey,
              ),
              onPressed: _pickImage,
              icon: const Icon(Icons.edit),
              label: const Text('Editieren')),
          const SizedBox(height: 40,),
          Text(name, style: studProfileTextStyle,),
        Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const SizedBox(width: 8.0, height: 45.0),
                // add some spacing between the icon and the text
                Text(email, style: studProfileRoleStyle),
                const SizedBox(width: 8.0, height: 45.0),
                // add some spacing between the icon and the text
                 Icon((type == "Student") ? Icons.school : Icons.business),
                Text(type, style: studProfileRoleStyle),
              ],
            ),
        )


        ],
      ),
    );
  }

  Future<void> _pickImage() async {
    final ImagePicker imagePicker = ImagePicker();
    final XFile? image = await imagePicker.pickImage(source: ImageSource.gallery);
    final bytes = await image?.readAsBytes();
    setState(() {
      // TODO: Load Image to Azure?!
      _imageProvider = Image.memory(bytes!).image;
    });
  }
}
